namespace MVCDemoSoft2.RequestParam
{
    public class BookParam
    {
        public int Id{get;set;}
        public string BookName { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public decimal Price { get; set; }
        public string ISBN { get; set; }
    }
}