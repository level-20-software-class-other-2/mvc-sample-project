using Microsoft.EntityFrameworkCore;
using MVCDemoSoft2.Models;

namespace MVCDemoSoft2.Db
{
    public class DemoSoft2 : DbContext
    {
        string connectString = "server=localhost;database=DemoSoft2;uid=postgres;pwd=qq_112358;";
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {

            builder.UseNpgsql(connectString);
        }



        public DbSet<BookLibrary> BookLibrary { get; set; }
        public DbSet<Book> Book { get; set; }
    }
}