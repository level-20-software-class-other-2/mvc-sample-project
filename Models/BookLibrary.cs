using System.Collections.Generic;

namespace MVCDemoSoft2.Models
{
    public class BookLibrary:BaseEntity
    {
        public string LibraryName { get; set; }

        public string Address { get; set; }

        public string Master { get; set; }

        public IEnumerable<Book> Books{get;set;}
    }
}