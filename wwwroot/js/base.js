$(function () {
  // 事件绑定 查找按钮
  $('#btnQuery').click(function () {
    console.log('你点了查找')
    let keywork = $('[name=keyword]').val()
    let obj = {
      keywork,
    }
    console.log(obj)
    // 利用jQuery 发起http post请求，请求返回json数据
    $.post('/book/query', obj, function (res) {
      console.log(res)
      // 移除表格中队标量外的所有行
      $('.tableData').html('')
      // 根据表格中的行模板，遍历返回的数据集，将结果数据安放到合适的位置，再将处理好的模板插入到表格中
      res.forEach((item) => {
        let html = `
                    <tr class="tableData">
                        <td>${item.id}</td>
                        <td>${item.bookName}</td>
                        <td>${item.author}</td>
                        <td>${item.price}</td>
                        <td>${item.publisher}</td>
                        <td>${item.isbn}</td>
                        <td>
                            <input type="button" value="编辑" onclick="btnEdit(${item.id})">
                            <input type="button" value="删除" onclick="btnDel(${item.id})">
                        </td>
                    </tr>
                `
        $('#tb').append(html)
      })
    })
  })
  // 事件绑定 新增按钮
  $('#btnAdd').click(function () {
    console.log('你点新增')
    window.location.href = '/book/createOrEdit'
  })

  // 事件绑定  保存 用于新增或者编辑
  $('#btnSave').click(function () {
    let id = $('[name=id]').val()
    let bookName = $('[name=bookName]').val()
    let author = $('[name=author]').val()
    let price = $('[name=price]').val()
    let publisher = $('[name=publisher]').val()
    let isbn = $('[name=isbn]').val()

    let obj = {
      id,
      bookName,
      author,
      price,
      publisher,
      isbn,
    }
    console.log(obj)
    // 保存成功，使用回调函数执行一些逻辑 比如跳转书本控制器首页
    $.post('/book/save', obj, function () {
      window.location.href = '/book/index'
    })
  })
  // 事件绑定 取消保存
  $('#btnCancel').click(function () {
    window.location.href = '/book/index'
  })
})
// 绑定编辑按钮，传递id值
function btnEdit(id) {
  console.log(`你点编辑按钮，id为${id}`)
  window.location.href = `/book/createOrEdit/${id}`
}
// 绑定删除按钮，传递id值
function btnDel(id) {
  console.log(`你点删除按钮，id为${id}`)
  let msg = '确认删除吗？'
  if (confirm(msg)) {
    window.location.href = `/book/delete/${id}`
  }
}
